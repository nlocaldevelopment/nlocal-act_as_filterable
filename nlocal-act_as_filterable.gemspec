# coding: utf-8
lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "nlocal/act_as_filterable/version"

Gem::Specification.new do |gem|
  gem.name          = "nlocal-act_as_filterable"
  gem.version       = Nlocal::ActAsFilterable::VERSION
  gem.authors       = ["Daniel Prado","Raul_Cabrera"]
  gem.email         = ["raul.cabrera@publicar.com"]

  gem.summary       = %q{Encapsultates Nlocal filterable module.}
  gem.description   = %q{This gem add filter scopes dinamically.}
  gem.homepage      = "https://bitbucket.org/nlocaldevelopment/nlocal-act_as_filterable"
  gem.license       = "MIT"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if gem.respond_to?(:metadata)
    gem.metadata["allowed_push_host"] = "TODO: Set to 'http://mygemserver.com'"
  else
    raise "RubyGems 2.0 or newer is required to protect against public gem pushes."
  end

  gem.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  gem.bindir        = "exe"
  gem.executables   = gem.files.grep(%r{^exe/}) { |f| File.basename(f) }
  gem.require_paths = ["lib"]

  gem.add_development_dependency "bundler", ">= 1.15"
  gem.add_development_dependency "rake", ['>= 0.8.7']
  gem.add_development_dependency "pry", ['>= 0.9.0']
  gem.add_development_dependency "rspec", ">= 3.0"
  # gem.add_runtime_dependency "pry"
  gem.add_runtime_dependency "rails", ['>= 4.1.8']
end
