require "nlocal/act_as_filterable/base"
require "rails"

module Nlocal
  module ActAsFilterable
    class Railtie < ::Rails::Railtie

      initializer "nlocal-act_as_filterable.active_record" do
        ActiveSupport.on_load(:active_record) do
          puts "Extending #{self} with Nlocal::ActAsFilterable"
          # ActiveRecord::Base gets a method that allows models to include the new behavior
          extend Nlocal::ActAsFilterable::Base
        end
      end

    end
  end
end
