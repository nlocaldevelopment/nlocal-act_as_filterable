require "nlocal/act_as_filterable/version"

module Nlocal
  module ActAsFilterable
    extend ActiveSupport::Concern

    ALLOWED_AGGREGATIONS = ["average", "sum", "maximum", "minimum", "count"]
    ALLOWED_PREDICATIONS = ["lt","gt","eq","lteq","gteq", "like"]
    SEARCHABLE_TYPES = [:string, :text, :jsonb]

    included do
      self.class_attribute :filters
      self.filters = []

      #ORDER
      scope :by_order, ->(order) {
        sql=[]
        order.each do |key,value|
          sql<< "#{key} #{value.upcase}"
        end
        order(sql.compact.join(','))
      }
      #AGGREGATION
      scope :by_group, ->(group_attr) {select(group_attr).group(group_attr)}
    end

    def exchange_attribute
      if self.class.exchange_attr_source != self.class
         self.try(self.class.arel_exchange_column.relation.name.singularize.to_sym).send(self.class.exchange_attribute)
      else
         self.send(self.class.exchange_attribute)
      end
    end

    module ClassMethods
      def filter(filtering_params)
        results = self.where(nil)
        filtering_params.slice(*self.filters).each do |key, value|
          value= value.to_unsafe_h if value.is_a?(ActionController::Parameters)
          results = results.public_send(("by_"+key.to_s).to_sym, value) if value.present? && self.respond_to?(("by_"+key.to_s).to_sym)
        end
        results
      end

      def scope(name, body, &block)
        self.filters += [name.to_s.sub(/^by_/, '').to_sym]
        super
      end

      def strict_attr_scopes(*attrs)
        attrs.each do |key|
          self.scope "by_#{key.to_s}".to_sym, ->(args) {
            result = where("#{key}".to_sym => args)
          }
        end
      end

      def comparable_attr_scopes (*attrs)
        attrs.each {|key|
          self.scope "by_#{key.to_s}".to_sym, ->(args) {
            result = where("true")
            if args.is_a? (Hash)
              args.each { |k,v|
                if self::ALLOWED_PREDICATIONS.include? k.to_s
                  case k.to_s
                  when 'like'
                    if SEARCHABLE_TYPES.include? (type= self.columns_hash[key.to_s].type)
                      if type == :jsonb
                        result= result.where(Arel::Nodes::NamedFunction.new("CAST",
                                             [arel_table[key].as("TEXT")]).matches("%#{v}%"))
                      else
                        result= result.where( arel_table[key].matches("%#{v}%"))
                      end
                    end
                  else
                    result= result.where( arel_table[key].send(k.to_sym, v))
                  end
                end
              }
            else
              result= result.where("#{key}" => args)
            end
            result
          }
        }
      end

      def ratio_aggregator_scope (key, column1, column2, function: lambda{ |op1,op2|"#{op1}/#{op2}::float"}, is_currency: false)
       self.scope "by_#{key.to_s}_aggregator".to_sym, ->(aggregator){
         if self::ALLOWED_AGGREGATIONS.include? aggregator
           case aggregator.to_sym
           when :average
              result= is_currency && (arel_exchange_column.relation != arel_table) ? joins(arel_exchange_column.relation.name.singularize.to_sym) : where("true")
              op1 = if is_currency
                      (arel_exchange_column * arel_table[column1.to_sym]).sum.to_sql
                    else
                       arel_table[column1.to_sym].sum.to_sql
                    end
              op2=  arel_table[column2.to_sym].send(aggregator.to_sym).to_sql
              result.select("CASE
                             WHEN 0 = #{op2} THEN 0.0
                             ELSE #{function.call(op1,op2)}
                             END AS #{key.to_s}"
                           )
           else
             select(arel_table[key.to_sym].send(aggregator.to_sym).as(key.to_s))
           end
         end
       }
      end

      def aggregator_scope (key, is_currency: false)
         self.scope "by_#{key.to_s}_aggregator".to_sym, ->(aggregator){
             if self::ALLOWED_AGGREGATIONS.include? aggregator
               result= is_currency && (arel_exchange_column.relation != arel_table) ? joins(arel_exchange_column.relation.name.singularize.to_sym) : where("true")
               column= if is_currency
                  (arel_exchange_column * arel_table[key.to_sym]).send(aggregator.to_sym)
               else
                   arel_table[key.to_sym].send(aggregator.to_sym)
               end
               result.select(column.as("#{key.to_s}"))
             end
         }
      end

      def aggregator_attr_scopes (*attrs, is_currency: false)
        attrs.each {|key|
         self.aggregator_scope(key, is_currency: is_currency)
        }
      end

      def arel_exchange_column
        exchange_attr_source.arel_table[exchange_attr_source.exchange_attribute]
      end

      def exchange_attribute
        exchange_attr_source != self ? exchange_attr_source.exchange_attribute : :reference_exchange_ratio
      end

      def exchange_attr_source
        self
      end

    end
  end
end
