require "nlocal/act_as_filterable/models/concerns/act_as_filterable"

module Nlocal
  module ActAsFilterable
    module Base
      def act_as_filterable
        include Nlocal::ActAsFilterable
      end
    end
  end
end